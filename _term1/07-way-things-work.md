---
title: The way things work
layout: "page"
order: 7
---
### An introduction to physical computing by hacking everyday objects   

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/fpNoHtF_S.jpg)  

[The way things work](https://hackmd.io/ys7lcCDJSeq69meSeGnIXg?view)  

### Faculty
Guillem Camprodon   
Victor Barberan  
Oscar Gonzalez  

### Syllabus and Learning Objectives
We spend our lives interacting with objects and interfaces who’s underlying technology we hardly understand not merely due to their complexity but also because they were intended to be closed by design.

Through the idea of hacking, we will explore the internal components building everyday objects, from coffee machines to wi-fi networks, while learning how to use open software and hardware tools to change the way they work and interface with the world.

We will use tools such as Arduino, Raspberry Pis and Python as an introduction to the work you will later develop during the Fabacademy course.  

### Total Duration
Classes: 15 hours (10am to 1pm)  
Student work hours: 15 hours (3 to 6pm)  
Total hours per student: 30 hours (Monday to Friday)  

### Structure and Phases
* *TBC*     

### Background Research Material
[The way things work - References](https://hackmd.io/ys7lcCDJSeq69meSeGnIXg?view#References)  

### Requirements for the Students
All the electronics materials as development boards, sensors and actuators will be provided during the workshop. However, we encourage those already owning an Arduino Kit, a Raspberry Pi, etc. to bring it.  
Bring in your laptop and any prototyping tools you have around such as a cutter, tape, markers, screwdrivers…  
Do you have any old appliance at home you would like to take apart? Bring them, too!  

### GUILLEM CAMPODRON - VICTOR BARBERAN - OSCAR GONZALEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/Captura_de_pantalla_2018-11-08_a_las_17.48.14.png)  
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/Captura_de_pantalla_2018-11-08_a_las_17.53.31.png)  

### Email Address
<pc.mdef@iaac.net>  

### Professional BIO
**Guillem Camprodon** is an interaction designer working on the intersection between the Internet of Things and Digital Fabrication.  
His wide knowledge of internet technologies and his training as a product designer makes him an expert on developing Internet of Things projects. He currently holds a researcher position at the Institute for Advanced Architecture of Catalonia (IAAC) and Fab Lab Barcelona working for companies such Endesa or Cisco. He is also a regular advisor for many projects as a tangible interaction expert and he regularly teaches workshops on open-source electronics and programming for Architects and Designers.  
He is one of the core members of the Smart Citizen project, a global open-source environmental monitoring platform. 

**Víctor Barberán** is an Industrial Designer with more than 20 years experience in developing custom technology for multidisciplinary art and science projects.  
Throughout his career, Victor Barberán has worked in electronics design, software development, data analysis, modelling and animation, and also in digital postproduction.  
Currently, Victor works as part of the Fab Lab Barcelona team doing research and development within the Smart Citizen project and teaches at the Bachelor of Smart Design at the ESDI School of Design.