---
title: Faculty
layout: page
---


### Directors:

Tomas Diez, Director – Design Studio leader – urban design and digital fabrication

Oscar Tomico, Co-director – Design Studio leader – design engineering and integrated products

### Faculty:

* Guillem Camprodon, Computer Science, Tools and Platforms (Fab Lab BCN)
* Victor Barberán, Computer Science, Tools and Platforms (Fab Lab BCN)
* Ingi Freyr Guðjónsson, Product Design and Digital Fabrication (Fab Lab BCN)
* Anastasia Pistofidou, Materials and Embedded Technologies (Textile Lab at Fab Lab BCN)
* Jonathan Minchin, Design and Sustainability (Fab Lab BCN)
* Nuria Conde Pueyo, Synthetic and Computational Biology (Universitat Pompeu Fabra at PRBB)
* Ramon Sangüesa, Artificial intelligence and Machine Learning (Elisava)
* Lucas Peña, Artificial intelligence and Machine Learning
* Elisabet Roselló Román, Strategic Innovation
* Santi Fuentemilla, Digital Fabrication (Fab Academy at Fab Lab BCN)
* Xavier Domínguez, Digital Fabrication (Fab Academy at Fab Lab BCN)
* Eduardo Chamorro, Digital Fabrication (Fab Academy at Fab Lab BCN)
* Mariana Quintero, Media Arts, Media Studies, Digital Literacy & Embodied Cognition
* Thomas Duggan, Materials and generative design (Thomas Duggan Studio)
* Mara Balestrini, New business models, user engagement, HCI (Ideas for Change)
* Jose Luis de Vicente, Digital Culture, Innovation and New Media Art (Sonar+D)
* Ron Wakkary, Kristina Andersen, Angella Mackey, David McCallum, Interaction Design, Industrial Design, Wearables, Fashion, Media Art and Design Research (Eindhoven University of Technology)
* Mercé Rua, Markel Cormenzana, Transition Design (Holon)
* Andrés Colmenares, Speculative Research, Internet Post-technological Future (IAM)

### Special Faculty & Advisors:

* Neil Gershenfeld, Center for Bits and Atoms at MIT
* Indy Johar, Dark Matter Labs
* Primavera de Filippi, CRNS – France
* James Tooze, Royal College of Arts and Design
* Liz Corbin, Institute of Making, UCL
* Heather Corcoran, Kickstarter
* Usman Haque, Umbrellium
* Mette Bak Andersen, Material Design (Material Design Lab at KEA)
* Jordi Serra del Pino, Center for Postnormal Policy & Futures Studies
* Saúl Baeza, DOES Work – Elisava
* Efraín Foglia, guifi.net
