---
title: Designing for 2050
layout: "page"
order: 2
---
### Collective Imaginations: Stories for the next billion seconds

![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/6003840d13e2-earth.gif)

### Faculty
Andrés Colmenares  

### Syllabus and Learning Objectives
What futures do we want to remember by 2050? What stories do we need to imagine collectively today to be able to shape better tomorrows 1 billion seconds from now? In fact, what is a billion? How can a thought experiment as reimagining the meaning the internet(s) and thinking of it in plural, empower us and change the way we relate between humans, non-humans and with the Planet? What if we start thinking of ourselves as internet citizens rather than internet users?

In this course, participants will work in collaborative ways to design fictional narratives around the concept of internet citizenships and post-technological futures, integrating their final projects and research insights, using as a 1 billion second time horizon together with key insights from the research themes that IAM has been exploring in the last years, in particular from 2019’s theme: The Quantumness of Archipelagos, a ‘what if?’ remix of ideas coming from philosophy, geography, queer theory and quantum physics, shaped as an experimental thinking tool to deal with the complexity of our realities and a lens through which we can imagine alternatives, collectively.

The course will run as an experimental studio during 12 hours (split into 4 sessions), where participants will need self-organise to achieve a collective goal, refine the key questions shaping their projects, practice consensus, reflection, self-grading and shared learnings that will inform the individual projects and also the group performance. The main goal of this format is not only the quality of the final output but most importantly the collective learnings from running a studio, working in collaborative ways and developing interconnected mindsets around three main ideas: critical optimism, long-term strategies and planetary narratives.  

### Total Duration
12 hours (4x 3hrs sessions)  
April 25-26 + May 2-3 from 3 to 6 pm  
Estimated student work time: 9 hours  

### Structure and Phases
* *Session I (April 25): Introduction & Studio Challenge*     
In Randomness We Trust (presentation)
Introduction to IAM’s research approach, philosophy, principles and 2050 mission
What is a billion? (provocation)
Challenge presentation
Setting up the Studio
Consensus experiment    
* *Session II (April 26): Sharing Perspectives*    
“Hello Stranger” survey (Fieldwork in teams)
Group discussion
Meta-Story draft  
* *Session III (May 2): Shaping Perceptions*    
Studio session (work in teams)
Story drafts  
* *Session IV (May 3): Screening & Reflection*    
Screenings
Feedback session
Reflection / Self-grading   

### Output
A pilot for an alternative version of 'Black Mirror' made-up of 5 episodes.  

### Grading Method
Collective self-grading: 100% the group will decide collectively their final grade for this course based on their reflections after the studio experience.  

### Bibliography
Suggested related books:  
Coupland, Douglas; Ulrich Obrist, Hans; Basar, Shumon (2015) The Age of Earthquakes: A Guide to the Extreme Present. Blue Rider Press  
Latour, Bruno (2018) Down to Earth: Politics in the New Climatic Regime. Polity  
Demos, T.J (2016) Decolonizing Nature: Contemporary Art and the Politics of Ecology. Sternberg Press  
Sutela, Jenna (2017) Orgs: From Slime Mold to Silicon Valley and Beyond. Garret Publications  
Srnicek, Nick; Williams Alex (2015) Inventing the Future: Postcapitalism and a World Without Work. Verso Books  

### Requirements for the Students
Trust.  

### ANDRÉS COLMENARES  
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/3cd0b5eea997-20190223_IAMxIntern_004.jpg)

### Affiliation
IAM co-founder  

### Email Address
<andres@internetagemedia.com>

### Personal Website
[IAM](https://iam-internet.com)

### Twitter Account
@colmenares  

### Professional BIO
Andres Colmenares is the co-founder of IAM, the alternative think-tank, consultancy and community, exploring the evolution of internet cultures and the influence of digital technologies in the futures of citizens and the Planet. 

IAM is collectively understanding, interconnecting and shaping post-technological futures: of everything, for everyone and everywhere.

Since 2015, together with his partner Lucy Black-Swan, they are developing unconventional research, curatorial & commissioning projects through creative partnerships with cultural institutions, universities and media companies as Tate, University of Arts London or the BBC, along with a number of experimental initiatives, including events, videos, learning journeys and publications, always expanding and following the principles stated in the IAM manifesto.

Their approach is built on over 10 years dedicated to explore speculative research themes, methods and narratives, looking at how the evolution of the internet as cultures, is shaping the futures of everything, creating tools and experiences to empower people through critical, planetary and long-term thinking.

On the public side, Andres also writes opinion articles for publications as CRACK Magazine or LS:N Global and has been invited to speak at events as the Digitally Engaged Learning Conference in Toronto, Lift Conference in Geneva, Calvert Forum in Kazan, Youth Marketing Day in Helsinki, Pecha Kucha Night and World Youth Student Travel Conference in Barcelona, and has been selected to participate in hackathons and design sprints in Doha (CANVAS Open-innovation Hackathon by Al-Jazeera), Berlin (Hack/Hackers Connect by Google News Lab) and Stockholm (Future of banking sprint by Swedbank) He is also a guest lecturer in University of Arts London, Istituto Europeo di Design, Blanquerna, LaSalle College International and ELISAVA School of Design & Engineering of Barcelona. And now at IAAC :)
